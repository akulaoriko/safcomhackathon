<?php return array (
  'parameters' => 
  array (
    'database_host' => 'localhost',
    'database_port' => '',
    'database_name' => 'db_hackathon',
    'database_user' => 'root',
    'database_password' => '',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => '4dZUzvpt2foImVaKiachq0Tpj6c2iTNhSXtwqzsUuRsPpMcUCWR5ENEh',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2019-02-15',
    'locale' => 'en-US',
    'cookie_key' => '1afAj3Ltpn4GC4BVAQPnKB5ubzd4wq3Zpy74VmahyTRX488rcoXcRwVq',
    'cookie_iv' => 'erCQYi33',
    'new_cookie_key' => 'def00000e78bc9b2bbf493b125209452245e15a35cfcaa2585c5cefe57074efc129af0966b78c9656241cd929f3cb92de5475e7df5f459180b0e820e71262cd69313b66d',
  ),
);
