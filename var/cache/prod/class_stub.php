<?php

class Cookie extends CookieCore {};
class SpecificPriceRule extends SpecificPriceRuleCore {};
class Hook extends HookCore {};
class ValidateConstraintTranslator extends ValidateConstraintTranslatorCore {};
class HelperImageUploader extends HelperImageUploaderCore {};
class HelperKpi extends HelperKpiCore {};
class HelperUploader extends HelperUploaderCore {};
class HelperTreeCategories extends HelperTreeCategoriesCore {};
class HelperView extends HelperViewCore {};
class HelperForm extends HelperFormCore {};
class HelperShop extends HelperShopCore {};
class HelperList extends HelperListCore {};
class HelperCalendar extends HelperCalendarCore {};
class HelperKpiRow extends HelperKpiRowCore {};
class Helper extends HelperCore {};
class HelperOptions extends HelperOptionsCore {};
class HelperTreeShops extends HelperTreeShopsCore {};
class Customer extends CustomerCore {};
class Manufacturer extends ManufacturerCore {};
class TreeToolbarLink extends TreeToolbarLinkCore {};
abstract class TreeToolbarButton extends TreeToolbarButtonCore {};
class TreeToolbar extends TreeToolbarCore {};
class TreeToolbarSearchCategories extends TreeToolbarSearchCategoriesCore {};
class Tree extends TreeCore {};
class TreeToolbarSearch extends TreeToolbarSearchCore {};
class Category extends CategoryCore {};
class ConfigurationKPI extends ConfigurationKPICore {};
class ConnectionsSource extends ConnectionsSourceCore {};
class CMSRole extends CMSRoleCore {};
class ProductAssembler extends ProductAssemblerCore {};
class CustomizationField extends CustomizationFieldCore {};
class LocalizationPack extends LocalizationPackCore {};
class CustomerAddress extends CustomerAddressCore {};
class Link extends LinkCore {};
class AttributeGroup extends AttributeGroupCore {};
class FileUploader extends FileUploaderCore {};
class Contact extends ContactCore {};
class CMSCategory extends CMSCategoryCore {};
class Risk extends RiskCore {};
class Product extends ProductCore {};
class Zone extends ZoneCore {};
class SupplierAddress extends SupplierAddressCore {};
class RangePrice extends RangePriceCore {};
class RangeWeight extends RangeWeightCore {};
class Country extends CountryCore {};
class Gender extends GenderCore {};
class Mail extends MailCore {};
class CacheMemcache extends CacheMemcacheCore {};
class CacheXcache extends CacheXcacheCore {};
abstract class Cache extends CacheCore {};
class CacheMemcached extends CacheMemcachedCore {};
class CacheApc extends CacheApcCore {};
class ProductSale extends ProductSaleCore {};
class Attachment extends AttachmentCore {};
class GroupReduction extends GroupReductionCore {};
class RequestSql extends RequestSqlCore {};
class QqUploadedFileForm extends QqUploadedFileFormCore {};
class PrestaShopCollection extends PrestaShopCollectionCore {};
class Guest extends GuestCore {};
abstract class HTMLTemplate extends HTMLTemplateCore {};
class HTMLTemplateOrderSlip extends HTMLTemplateOrderSlipCore {};
class HTMLTemplateDeliverySlip extends HTMLTemplateDeliverySlipCore {};
class HTMLTemplateOrderReturn extends HTMLTemplateOrderReturnCore {};
class PDF extends PDFCore {};
class HTMLTemplateSupplyOrderForm extends HTMLTemplateSupplyOrderFormCore {};
class HTMLTemplateInvoice extends HTMLTemplateInvoiceCore {};
class PDFGenerator extends PDFGeneratorCore {};
class TabLang extends TabLangCore {};
class OrderStateLang extends OrderStateLangCore {};
class MetaLang extends MetaLangCore {};
class DataLang extends DataLangCore {};
class ThemeLang extends ThemeLangCore {};
class GroupLang extends GroupLangCore {};
class SupplyOrderStateLang extends SupplyOrderStateLangCore {};
class AttributeLang extends AttributeLangCore {};
class GenderLang extends GenderLangCore {};
class OrderReturnStateLang extends OrderReturnStateLangCore {};
class AttributeGroupLang extends AttributeGroupLangCore {};
class ContactLang extends ContactLangCore {};
class ConfigurationLang extends ConfigurationLangCore {};
class CategoryLang extends CategoryLangCore {};
class OrderMessageLang extends OrderMessageLangCore {};
class FeatureLang extends FeatureLangCore {};
class RiskLang extends RiskLangCore {};
class QuickAccessLang extends QuickAccessLangCore {};
class CmsCategoryLang extends CmsCategoryLangCore {};
class CarrierLang extends CarrierLangCore {};
class StockMvtReasonLang extends StockMvtReasonLangCore {};
class ProfileLang extends ProfileLangCore {};
class FeatureValueLang extends FeatureValueLangCore {};
class Tag extends TagCore {};
class CustomerAddressPersister extends CustomerAddressPersisterCore {};
class CustomerForm extends CustomerFormCore {};
class CustomerFormatter extends CustomerFormatterCore {};
class CustomerAddressFormatter extends CustomerAddressFormatterCore {};
class FormField extends FormFieldCore {};
class CustomerAddressForm extends CustomerAddressFormCore {};
class CustomerPersister extends CustomerPersisterCore {};
abstract class AbstractForm extends AbstractFormCore {};
class CustomerLoginFormatter extends CustomerLoginFormatterCore {};
class CustomerLoginForm extends CustomerLoginFormCore {};
class Image extends ImageCore {};
class Cart extends CartCore {};
class PhpEncryptionLegacyEngine extends PhpEncryptionLegacyEngineCore {};
class Attribute extends AttributeCore {};
class Feature extends FeatureCore {};
class PrestaShopBackup extends PrestaShopBackupCore {};
class Notification extends NotificationCore {};
class PhpEncryptionEngine extends PhpEncryptionEngineCore {};
class State extends StateCore {};
class Tools extends ToolsCore {};
class Referrer extends ReferrerCore {};
class CartRule extends CartRuleCore {};
class Connection extends ConnectionCore {};
abstract class ModuleGrid extends ModuleGridCore {};
abstract class ModuleGraphEngine extends ModuleGraphEngineCore {};
abstract class CarrierModule extends CarrierModuleCore {};
abstract class ModuleGraph extends ModuleGraphCore {};
abstract class Module extends ModuleCore {};
abstract class ModuleGridEngine extends ModuleGridEngineCore {};
class Address extends AddressCore {};
class TranslatedConfiguration extends TranslatedConfigurationCore {};
class LinkProxy extends LinkProxyCore {};
abstract class AbstractCheckoutStep extends AbstractCheckoutStepCore {};
class CheckoutProcess extends CheckoutProcessCore {};
class CheckoutPersonalInformationStep extends CheckoutPersonalInformationStepCore {};
class CheckoutAddressesStep extends CheckoutAddressesStepCore {};
class CartChecksum extends CartChecksumCore {};
class ConditionsToApproveFinder extends ConditionsToApproveFinderCore {};
class CheckoutSession extends CheckoutSessionCore {};
class DeliveryOptionsFinder extends DeliveryOptionsFinderCore {};
class PaymentOptionsFinder extends PaymentOptionsFinderCore {};
class CheckoutPaymentStep extends CheckoutPaymentStepCore {};
class AddressValidator extends AddressValidatorCore {};
class CheckoutDeliveryStep extends CheckoutDeliveryStepCore {};
class Tab extends TabCore {};
class JavascriptManager extends JavascriptManagerCore {};
class JsMinifier extends JsMinifierCore {};
abstract class AbstractAssetManager extends AbstractAssetManagerCore {};
class CssMinifier extends CssMinifierCore {};
class CccReducer extends CccReducerCore {};
class StylesheetManager extends StylesheetManagerCore {};
class Windows extends WindowsCore {};
class Store extends StoreCore {};
class Dispatcher extends DispatcherCore {};
class FileLogger extends FileLoggerCore {};
abstract class AbstractLogger extends AbstractLoggerCore {};
class ConfigurationTest extends ConfigurationTestCore {};
class CMS extends CMSCore {};
class SupplyOrderReceiptHistory extends SupplyOrderReceiptHistoryCore {};
class StockManager extends StockManagerCore {};
class Stock extends StockCore {};
class StockAvailable extends StockAvailableCore {};
class SupplyOrderState extends SupplyOrderStateCore {};
class StockManagerFactory extends StockManagerFactoryCore {};
class StockMvtReason extends StockMvtReasonCore {};
class StockMvt extends StockMvtCore {};
class SupplyOrderDetail extends SupplyOrderDetailCore {};
class StockMvtWS extends StockMvtWSCore {};
class WarehouseProductLocation extends WarehouseProductLocationCore {};
class SupplyOrderHistory extends SupplyOrderHistoryCore {};
class SupplyOrder extends SupplyOrderCore {};
class Warehouse extends WarehouseCore {};
abstract class StockManagerModule extends StockManagerModuleCore {};
class DbPDO extends DbPDOCore {};
class DbMySQLi extends DbMySQLiCore {};
class DbQuery extends DbQueryCore {};
abstract class Db extends DbCore {};
class ManufacturerAddress extends ManufacturerAddressCore {};
class Configuration extends ConfigurationCore {};
class Carrier extends CarrierCore {};
class Chart extends ChartCore {};
class Validate extends ValidateCore {};
class Combination extends CombinationCore {};
class DateRange extends DateRangeCore {};
class PhpEncryption extends PhpEncryptionCore {};
class AddressFormat extends AddressFormatCore {};
abstract class ObjectModel extends ObjectModelCore {};
class Customization extends CustomizationCore {};
class Profile extends ProfileCore {};
class Curve extends CurveCore {};
class Access extends AccessCore {};
class Language extends LanguageCore {};
class Employee extends EmployeeCore {};
class Page extends PageCore {};
class PrestaShopException extends PrestaShopExceptionCore {};
class PrestaShopModuleException extends PrestaShopModuleExceptionCore {};
class PrestaShopDatabaseException extends PrestaShopDatabaseExceptionCore {};
class PrestaShopPaymentException extends PrestaShopPaymentExceptionCore {};
class PrestaShopObjectNotFoundException extends PrestaShopObjectNotFoundExceptionCore {};
class SpecificPrice extends SpecificPriceCore {};
class QqUploadedFileXhr extends QqUploadedFileXhrCore {};
class Pack extends PackCore {};
class PrestaShopLogger extends PrestaShopLoggerCore {};
class Supplier extends SupplierCore {};
class Message extends MessageCore {};
class Delivery extends DeliveryCore {};
abstract class PaymentModule extends PaymentModuleCore {};
class Upgrader extends UpgraderCore {};
class SearchEngine extends SearchEngineCore {};
class SmartyDevTemplate extends SmartyDevTemplateCore {};
class SmartyCustom extends SmartyCustomCore {};
class TemplateFinder extends TemplateFinderCore {};
class SmartyResourceModule extends SmartyResourceModuleCore {};
class SmartyCustomTemplate extends SmartyCustomTemplateCore {};
class SmartyResourceParent extends SmartyResourceParentCore {};
class ImageType extends ImageTypeCore {};
abstract class ProductListingFrontController extends ProductListingFrontControllerCore {};
abstract class ModuleAdminController extends ModuleAdminControllerCore {};
abstract class ProductPresentingFrontController extends ProductPresentingFrontControllerCore {};
class ModuleFrontController extends ModuleFrontControllerCore {};
class AdminController extends AdminControllerCore {};
class FrontController extends FrontControllerCore {};
abstract class Controller extends ControllerCore {};
class Search extends SearchCore {};
class Translate extends TranslateCore {};
class Uploader extends UploaderCore {};
class CSV extends CSVCore {};
class Currency extends CurrencyCore {};
class TaxManagerFactory extends TaxManagerFactoryCore {};
class TaxCalculator extends TaxCalculatorCore {};
abstract class TaxManagerModule extends TaxManagerModuleCore {};
class TaxConfiguration extends TaxConfigurationCore {};
class TaxRulesTaxManager extends TaxRulesTaxManagerCore {};
class TaxRulesGroup extends TaxRulesGroupCore {};
class TaxRule extends TaxRuleCore {};
class Tax extends TaxCore {};
class Media extends MediaCore {};
class AddressChecksum extends AddressChecksumCore {};
class ProductDownload extends ProductDownloadCore {};
class Context extends ContextCore {};
class OrderInvoice extends OrderInvoiceCore {};
class OrderState extends OrderStateCore {};
class OrderReturn extends OrderReturnCore {};
class OrderDiscount extends OrderDiscountCore {};
class OrderDetail extends OrderDetailCore {};
class OrderPayment extends OrderPaymentCore {};
class OrderReturnState extends OrderReturnStateCore {};
class OrderCartRule extends OrderCartRuleCore {};
class OrderHistory extends OrderHistoryCore {};
class OrderSlip extends OrderSlipCore {};
class OrderCarrier extends OrderCarrierCore {};
class Order extends OrderCore {};
class OrderMessage extends OrderMessageCore {};
class QuickAccess extends QuickAccessCore {};
class CustomerThread extends CustomerThreadCore {};
class Alias extends AliasCore {};
class CustomerMessage extends CustomerMessageCore {};
class Shop extends ShopCore {};
class ShopUrl extends ShopUrlCore {};
class ShopGroup extends ShopGroupCore {};
class WarehouseAddress extends WarehouseAddressCore {};
class ProductPresenterFactory extends ProductPresenterFactoryCore {};
class Group extends GroupCore {};
class FeatureValue extends FeatureValueCore {};
class WebserviceOutputBuilder extends WebserviceOutputBuilderCore {};
class WebserviceOutputXML extends WebserviceOutputXMLCore {};
class WebserviceException extends WebserviceExceptionCore {};
class WebserviceKey extends WebserviceKeyCore {};
class WebserviceSpecificManagementImages extends WebserviceSpecificManagementImagesCore {};
class WebserviceOutputJSON extends WebserviceOutputJSONCore {};
class WebserviceRequest extends WebserviceRequestCore {};
class WebserviceSpecificManagementSearch extends WebserviceSpecificManagementSearchCore {};
class ProductSupplier extends ProductSupplierCore {};
class ImageManager extends ImageManagerCore {};
class Meta extends MetaCore {};
